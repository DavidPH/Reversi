#include "Reversi.h"
#define NO_OF_PLAYERS 2
#define MAX_LENGTH 25

int main()
{

    //Array containing the board status
    //Value of -1 for black and 1 for white. A value of 0 means no piece.
    int Board[8][8] = { 0 }; //Array is initialized with all values of 0;
    int No_Disks = 60;

    //Initial 4 pieces:
    Board[3][3] = 1;
    Board[4][4] = 1;
    Board[4][3] = -1;
    Board[3][4] = -1;
    //

    //Initialises the player struct
    struct PlayerStatus Players[NO_OF_PLAYERS];
    //Calls the functions
    initialisePlayer(Players);
    colourCount(Board,Players);
    displayStatus(Players,No_Disks);
    printBoard(Board);

    return 0;
}

