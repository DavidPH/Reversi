#include "Reversi.h"

#define NO_OF_PLAYERS 2

struct PlayerStatus initialisePlayer(struct PlayerStatus player[])
{
    char *type[2] = {"Black","White"};
    int i;
    for(i = 0;i < NO_OF_PLAYERS;i++)
    {
        printf("\nPlease enter the name for player %d:\n",i+1);
        if(i == 0)
            puts("Note: Player 1 will be black and will start the game:");
        //Gets the user name from the user.
        fgets(player[i].playerName,MAX_LENGTH,stdin);
        //Gets rid of the \n for neater printing
        player[i].playerName[strcspn( player[i].playerName,"\n")] = 0;
        //Starts at 0 disks as a separate function will count the number on the board.
        player[i].disksOnBoard = 0;
        //Assigns the player the colour; Player 1 is black, Player 2 is white.
        strcpy(player[i].colour,type[i]);
    }

}

struct PlayerStatus colourCount(int Board[8][8],struct PlayerStatus player[])
{
    int i,j;
    //Reset the number as it needs to be recounted. A move add or remove multiple disks.
    player[1].disksOnBoard = 0;
    player[2].disksOnBoard = 0;
    for(i = 0;i < 8;i++)
    {
        for(j = 0;j < 8;j++)
        {
            if(Board[i][j] == -1) {
                player[0].disksOnBoard++;
            }
            else if(Board[i][j] == 1) {

                player[1].disksOnBoard++;
            }
        }
    }
}

void displayStatus(struct PlayerStatus player[],int DiskstoPlay)
{
    int i;
    //Prints the status of the player.
    for(i = 0;i < NO_OF_PLAYERS;i++)
    {
        printf("\n\nPlayer %d:%s \nColour: %s",i+1,player[i].playerName,player[i].colour);
    }
    printf("\n\nScore\n%s %d:%d %s",player[0].colour,player[0].disksOnBoard,player[1].disksOnBoard,player[1].colour);
    printf("\nDisks available to play:%d",DiskstoPlay);
}


void printBoard(int Board[8][8]){

    //Prints the Board
    // Board[0][0] means x is 1 and y is 1.
    // If Board[i][j] == 1 then it prints an 'O' if == -1 it prints an 'X'. 0 means no piece.

    int i,j;
    int Xcount,Ycount; // X and Y coordinates
    printf("\n");
    for(j=0;j<8;j++) {
        printf("|---|"); //Prints the first line of the board.
    }
    printf("\n");
    //Every loop iteration prints 1 square only
    for(i =1,Xcount=1,Ycount=1;i<=64;i++,Xcount++){

        if (Xcount == 9) {  //Keeps track on which square and column the loop is in
            Ycount++;
            Xcount = 1;
            for(j=0;j<8;j++) {
                printf("|---|");
            }
            printf("\n");
        }

        //Prints a piece if there's one on the square being checked.
        if (checkForPiece(Board,Xcount,Ycount)) {
            if(Board[Xcount-1][Ycount-1] == -1){
                printf("| %c |",'B');
            }
            else if(Board[Xcount-1][Ycount-1] == 1){
                printf("| %c |",'W');
            }
        }
        //
        else {
            printf("|   |");
        }
        if (i % 8 == 0) {  //Every 8 squares it moves down a line
            printf("\n");
        }
    }
    for(j=0;j<8;j++) {
        printf("|---|"); //Prints the last line of the board
    }
    printf("\n");

}

bool checkForPiece(int Board[8][8],int Xcount, int Ycount){
//Used only in the printBoard function
//Checks if a piece should be printed.
    int i;
    int j;
    bool out=false;

    for(i=0; i< 8; i++) {
        for(j=0; j< 8; j++) {

            if ((Board[i][j] != 0 && i+1 == Xcount && j+1 == Ycount)){
                out = true;
            }

        }
    }
    return out;

}