#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define MAX_LENGTH 25
typedef struct PlayerStatus
{
    char playerName[MAX_LENGTH];
    int disksOnBoard;
    char colour[6];
};
struct PlayerStatus initialisePlayer(struct PlayerStatus player[]);

struct PlayerStatus colourCount(int Board[8][8],struct PlayerStatus player[]);

void displayStatus(struct PlayerStatus player[],int DiskstoPlay);

void printBoard(int Board[8][8]);

bool checkForPiece(int Board[8][8], int Xcount, int Ycount);
